# AppDashBoard

### Installation

#### Clone repo

``` bash
# clone the repo
$ git https://gitlab.com/seidor_app_scp/seidor-vue_js-appdashboard.git

# go into app's directory
$ cd seidor-vue_js-appDashBoard

# install app's dependencies
$ npm install

# install vue cli service
npm i @vue/cli-service



#### Usage


# serve with hot reload at localhost:8080
npm run serve

# build for production with minification
npm run build

# run linter
npm run lint

# run unit tests
npm run test:unit

# run e2e tests
npm run test:e2e

